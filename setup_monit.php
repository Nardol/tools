<?php
/**
 * Set up Monit for certain services on a server.
 * 
 * @author Thom Pol
 * @copyright 2022 ISPConfig - Thom Pol
 * @example setup_monit.php
 * @example setup_monit.php --alert-email=user@example.com
 * @see
 * 
 */

function generatePassword($length = 8, $use_special = false) {
    // Verfügbare Zeichen für Passwort
    $available = "abcdefghjkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ123456789";
    if($use_special == true) {
$available .= '%_/-_&+.<>';
    }

    if($length < 1) {
$length = 8;
    }
    $passwd = "";

    $force_special = ($use_special == true ? mt_rand(1, $length) : 0);

    for($i=1;$i<=$length;$i++) {
// Passwort mit zufälligen Zeichen füllen bis Länge erreicht
if($i == $force_special) {
    $passwd .= substr($available, mt_rand(57, strlen($available) - 1), 1);
} else {
    $passwd .= substr($available, mt_rand(0, strlen($available) - 1), 1);
}
    }

    // kreiertes Passwort zurückgeben
    return $passwd;
}

print('[INFO] Installing Monit' . "\n");
shell_exec('DEBIAN_FRONTEND="noninteractive" apt-get install -o Dpkg::Options::="--force-overwrite" -qq -y monit');
print('[INFO] Configuring Monit.' . "\n");
$alertEmail = getopt(null, ["name:"]);
// Set up main config
$monitrc = file_get_contents('/etc/monit/monitrc');
str_replace('set daemon 120', 'set daemon 60', $monitrc);
file_put_contents('/etc/monit/monitrc', $monitrc);

// Set up config files for each service that shall be monitored
$installedServices = array(
	'filesystem',
	'resources'
);

// Get all PHP versions that are installed
$existing_php_versions = array(
	'5.6',
	'7.0',
	'7.1',
	'7.2',
	'7.3',
	'7.4',
	'8.0',
	'8.1'
);
$installed_php_versions = array();
foreach ($existing_php_versions as $php_version) {
    $php_exists = shell_exec('which php' . $php_version);
    if ($php_exists != '') {
        $installed_php_versions[] = $php_version;
    }
}

foreach ($installed_php_versions as $curver) {
	$phpfpm = 'php' . $curver . '-fpm';
	array_push($installedServices, $phpfpm);
}

// Check for all services if they exist and if so, add them to services array
$supportedServices = array(
    'sshd',
    'crond',
    'mariadb',
    'memcached',
	'pure-ftpd-mysql',
    'fail2ban',
    'apache2',
    'nginx',
    'named',
    'postfix',
    'dovecot',
    'rspamd',
    'redis-server'
);

foreach ($supportedServices as $service) {
    $service_exists = shell_exec('which ' . $service);
    if ($service_exists != '') {
        $installedServices[] = $service;
    }
}

$services = array_unique($installedServices);

// Set config directories
$confAvailableDir = '/etc/monit/conf-available/';
$confEnabledDir = '/etc/monit/conf-enabled/';
// Put config files in conf available directory
foreach ($services as $service) {
    if ($service == 'apache2') {
        $conf='check process apache with pidfile /var/run/apache2/apache2.pid
    group apache
    start program = "/usr/bin/systemctl start apache2" with timeout 60 seconds
    stop program  = "/usr/bin/systemctl stop apache2"
    if failed port 80 protocol http then restart
    if failed port 443 then restart
    if 5 restarts within 5 cycles then timeout
    depend apache_bin
    depend apache_rc
            
check file apache_bin with path /usr/sbin/apache2
    group apache
    include /etc/monit/templates/rootbin
        
check file apache_rc with path /etc/init.d/apache2
    group apache
    include /etc/monit/templates/rootbin';
    }

    if ($service == 'nginx') {
        $conf='check process nginx with pidfile /var/run/nginx.pid
    group nginx
    start program = "/usr/bin/systemctl start nginx" with timeout 60 seconds
    stop program  = "/usr/bin/systemctl stop nginx"
    if failed port 80 protocol http then restart
    if failed port 443 then restart
    if 5 restarts within 5 cycles then timeout
    depend nginx_bin
    depend nginx_rc
     
check file nginx_bin with path /usr/sbin/nginx
    group nginx
    include /etc/monit/templates/rootbin
    
check file nginx_rc with path /etc/init.d/nginx
    group nginx
    include /etc/monit/templates/rootbin';
    }

    if ($service == 'mariadb') {
        if (file_exists('/etc/init.d/mariadb') && file_exists('/usr/sbin/mariadbd')) {
            $conf='check process mariadb with pidfile /var/run/mysqld/mysqld.pid
	group mysql
	start program = "/usr/bin/systemctl start mariadb" with timeout 60 seconds
	stop program = "/usr/bin/systemctl stop mariadb"
	if failed host 127.0.0.1 port 3306 protocol mysql then restart
	if failed unixsocket /var/run/mysqld/mysqld.sock protocol mysql for 3 times within 2 cycles then restart
	if 5 restarts within 5 cycles then timeout
	
	depend mysql_bin
	depend mysql_rc
	
check file mysql_bin with path /usr/sbin/mariadbd
	group mysql
   	include /etc/monit/templates/rootbin
		
check file mysql_rc with path /etc/init.d/mariadb
	group mysql
	include /etc/monit/templates/rootbin';
		} elseif (file_exists('/etc/init.d/mysql') && file_exists('/usr/sbin/mysqld')) {
            $conf='check process mariadb with pidfile /var/run/mysqld/mysqld.pid
	group mysql
	start program = "/usr/bin/systemctl start mariadb" with timeout 60 seconds
	stop program = "/usr/bin/systemctl stop mariadb"
	if failed host 127.0.0.1 port 3306 protocol mysql then restart
	if failed unixsocket /var/run/mysqld/mysqld.sock protocol mysql for 3 times within 4 cycles then restart
	if 5 restarts within 5 cycles then timeout
	
	depend mysql_bin
	depend mysql_rc
	
check file mysql_bin with path /usr/sbin/mysqld
	group mysql
   	include /etc/monit/templates/rootbin
		
check file mysql_rc with path /etc/init.d/mysql
	group mysql
	include /etc/monit/templates/rootbin';
		} else {
            $conf='check process mariadb with pidfile /var/run/mysqld/mysqld.pid
	group mysql
	start program = "/usr/bin/systemctl start mariadb" with timeout 60 seconds
	stop program = "/usr/bin/systemctl stop mariadb"
	if failed host 127.0.0.1 port 3306 protocol mysql then restart
	if failed unixsocket /var/run/mysqld/mysqld.sock protocol mysql for 3 times within 4 cycles then restart
	if 5 restarts within 5 cycles then timeout';
		}
	}


	foreach($installed_php_versions as $curver) {
        $phpfpm = 'php' . $curver . '-fpm';
        if ($service == $phpfpm) {
            $conf='check process ' . $phpfpm . ' with pidfile /var/run/php/' . $phpfpm . '.pid
	start program = "/usr/bin/systemctl start ' . $phpfpm .'" with timeout 60 seconds
	stop program  = "/usr/bin/systemctl stop ' . $phpfpm . '"
	if failed unixsocket /var/run/php/' . $phpfpm . '.sock then restart
	if 5 restarts within 5 cycles then timeout';
        }
	}

    if ($service == 'memcached') {
        $conf='check process memcached with pidfile /var/run/memcached/memcached.pid
    group memcached
	start program = "/usr/bin/systemctl start memcached"
	stop program = "/usr/bin/systemctl stop memcached"
	if failed host 127.0.0.1 port 11211 protocol memcache then restart
	if 5 restarts within 5 cycles then timeout
	
	depend memcache_bin
	depend memcache_rc
	
check file memcache_bin with path /usr/bin/memcached
	group memcached
    include /etc/monit/templates/rootbin
   
check file memcache_rc with path /etc/init.d/memcached
	group memcached
	include /etc/monit/templates/rootbin';
    }


    if ($service == 'pure-ftpd-mysql') {
        $conf='check process pure-ftpd-mysql with pidfile /var/run/pure-ftpd/pure-ftpd.pid
    start program = "/usr/bin/systemctl start pure-ftpd-mysql" with timeout 60 seconds
    stop program  = "/usr/bin/systemctl stop pure-ftpd-mysql"
    if failed port 21 protocol ftp then restart
    if 5 restarts within 5 cycles then timeout';
    }

    if ($service == 'fail2ban') {
		$conf='check process fail2ban with pidfile /var/run/fail2ban/fail2ban.pid
	start program = "/usr/bin/systemctl start fail2ban" with timeout 60 seconds
    stop  program = "/usr/bin/systemctl stop fail2ban"
    if failed unixsocket /var/run/fail2ban/fail2ban.sock then restart
    if 5 restarts within 5 cycles then timeout

check file fail2ban_log with path /var/log/fail2ban.log
	if match "ERROR|WARNING" then alert';
			}


	if ($service == 'sshd') {
        $conf='check process sshd with pidfile /var/run/sshd.pid
	group sshd
	start program = "/etc/init.d/ssh start"
	stop  program = "/etc/init.d/ssh stop"
	if failed host localhost port 22 with proto ssh then restart
	if 5 restarts with 5 cycles then timeout
	depend on sshd_bin
	depend on sftp_bin
	depend on sshd_rc
	depend on sshd_rsa_key
	depend on sshd_dsa_key
			 
check file sshd_bin with path /usr/sbin/sshd
	group sshd
	include /etc/monit/templates/rootbin

check file sftp_bin with path /usr/lib/openssh/sftp-server
	group sshd
	include /etc/monit/templates/rootbin
			 
check file sshd_rsa_key with path /etc/ssh/ssh_host_rsa_key
	group sshd
	include /etc/monit/templates/rootstrict

check file sshd_dsa_key with path /etc/ssh/ssh_host_dsa_key
	group sshd
	include /etc/monit/templates/rootstrict
			 
check file sshd_rc with path /etc/ssh/sshd_config
	group sshd
	include /etc/monit/templates/rootrc';
	}

    if ($service == 'crond') {
        $conf='check process crond with pidfile /var/run/crond.pid
	group system
	group crond
	start program = "/usr/bin/systemctl start cron" with timeout 60 seconds
	stop  program = "/usr/bin/systemctl stop cron"
	if 5 restarts with 5 cycles then timeout
	depend cron_bin
	depend cron_rc
	depend cron_spool
			 
check file cron_bin with path /usr/sbin/cron
	group crond
	include /etc/monit/templates/rootbin
		 
check file cron_rc with path "/etc/init.d/cron"
	group crond
	include /etc/monit/templates/rootbin
			 
check directory cron_spool with path /var/spool/cron/crontabs
	group crond
	if failed permission 1730 then unmonitor
	if failed uid root        then unmonitor
	if failed gid crontab     then unmonitor';
    }

    if ($service == 'named') {
        $conf='check process named with pidfile /var/run/named/named.pid
    start program = "/usr/bin/systemctl start named" with timeout 60 seconds
    stop program  = "/usr/bin/systemctl stop named"
    if failed port 53 use type udp protocol dns then restart
    if 5 restarts within 5 cycles then timeout';
    }

    if ($service == 'postfix') {
        $conf='check process postfix with pidfile /var/spool/postfix/pid/master.pid
	group mail
	group postfix
	start program = "/usr/bin/systemctl start postfix" with timeout 60 seconds
	stop  program = "/usr/bin/systemctl stop postfix"
	if failed host localhost port 25 with protocol smtp for 2 times within 2 cycles then restart
	if 5 restarts with 5 cycles then timeout
	depend master_bin
	depend postfix_rc
	depend postdrop_bin
	depend postqueue_bin
	depend master_cf
	depend main_cf
			 
check file master_bin with path /usr/lib/postfix/sbin/master
	group postfix
	include /etc/monit/templates/rootbin
			 
check file postdrop_bin with path /usr/sbin/postdrop
	group postfix
	if failed checksum        then unmonitor
	if failed permission 2555 then unmonitor
	if failed uid root        then unmonitor
	if failed gid postdrop    then unmonitor
			 
check file postqueue_bin with path /usr/sbin/postqueue
	group postfix
	if failed checksum        then unmonitor
	if failed permission 2555 then unmonitor
	if failed uid root        then unmonitor
	if failed gid postdrop    then unmonitor
			 
check file master_cf with path /etc/postfix/master.cf
	group postfix
	include /etc/monit/templates/rootrc
			 
check file main_cf with path /etc/postfix/main.cf
	group postfix
	include /etc/monit/templates/rootrc
			 
check file postfix_rc with path /etc/init.d/postfix
	group postfix
	include /etc/monit/templates/rootbin';
    }

    if ($service == 'dovecot') {
        $conf='check process dovecot with pidfile /var/run/dovecot/master.pid
    group mail
    start program = "/usr/bin/systemctl start dovecot" with timeout 60 seconds
    stop program = "/usr/bin/systemctl stop dovecot"
    #if failed host mail.yourdomain.tld port 993 type tcpssl sslauto protocol imap then restart
    if failed port 143 protocol imap then restart
    if 5 restarts within 5 cycles then timeout';
    }

	if ($service == 'rspamd') {
        $conf='check process rspamd
	matching \'rspamd: main process\'
	start program = "/usr/bin/systemctl start rspamd" with timeout 60 seconds
	stop program = "/usr/bin/systemctl start rspamd"
	
	if cpu is greater than 40% then alert
	if cpu > 60% for 4 cycles then alert
	if memory > 80% for 4 cycles then alert
	if totalmem > 1024 MB for 4 cycles then alert';
	}

    if ($service == 'redis-server') {
        $conf='check process redis-server with pidfile "/var/run/redis/redis-server.pid"
    start program = "/usr/bin/systemctl start redis-server"
    stop program = "/usr/bin/systemctl stop redis-server"
    if failed host 127.0.0.1 port 6379 protocol redis then restart
    if 5 restarts within 5 cycles then timeout';
    }

    if ($service == 'filesystem') {
        $conf='check filesystem rootfs with path /
    if space usage > 90% then alert
    if inode usage > 80% then alert';
    }

	if ($service == 'resources') {
		$conf='check system $HOST
	if loadavg (5min) > 3 then alert
    if loadavg (15min) > 1 then alert
    if memory usage > 80% for 6 cycles then alert
    if swap usage > 20% for 6 cycles then alert
    # Test the user part of CPU usage 
    if cpu usage (user) > 80% for 6 cycles then alert
    # Test the system part of CPU usage 
    if cpu usage (system) > 20% for 6 cycles then alert
    # Test the i/o wait part of CPU usage 
    if cpu usage (wait) > 80% for 4 cycles then alert
    # Test CPU usage including user, system and wait. Note that 
    # multi-core systems can generate 100% per core
    # so total CPU usage can be more than 100%
    if cpu usage > 200% for 4 cycles then alert';
	}

    if ($conf != '') {
        file_put_contents($confAvailableDir . $service, $conf);
    }
}

// Set up alert config
$optionsPassed = getopt(null, ["alert-email:"]);
if ($optionsPassed) {
    $monitEmail = $optionsPassed['alert-email'];
}
$service = 'alerts';
$conf = '#set mailserver localhost
#set mailserver smtp.example.com port 587
	#username "user@example.com" password "welcome"
				
#set alert admin@example.com

#set mail-format {
#	from:    Monit <monit@$HOST>
#	subject: Monit alert on $HOST -- $EVENT $SERVICE
#	message: $EVENT Service $SERVICE
#
#Date:        $DATE
#Action:      $ACTION
#Host:        $HOST
#Description: $DESCRIPTION
#  
#Your faithful employee,
#Monit
#}';
if (!empty($monitEmail)) {
	if (filter_var($monitEmail, FILTER_VALIDATE_EMAIL)) {
        $conf = 'set mailserver localhost
#set mailserver smtp.example.com port 587
	#username "user@example.com" password "welcome"
	#using tls

set alert ' . $monitEmail . '

#set mail-format {
#	from:    Monit <monit@$HOST>
#	subject: Monit alert on $HOST -- $EVENT $SERVICE
#	message: $EVENT Service $SERVICE
#
#Date:        $DATE
#Action:      $ACTION
#Host:        $HOST
#Description: $DESCRIPTION
#  
#Your faithful employee,
#Monit
#}';
	} else {
        print('[WARN] E-mail address for Monit alerts is invalid. Set up alerts manually in /etc/monit/conf-available/alerts' . "\n");
	};
}

file_put_contents($confAvailableDir . $service, $conf);

$services[] = $service;

// Configure main config with UI and admin + pass
$service = "webui";
$monitpw = '';
$monitpw = generatePassword(12);
$conf='set httpd port 2812 and
	#SSL ENABLE
	#PEMFILE /usr/local/ispconfig/interface/ssl/ispserver.pem
	allow admin:' . $monitpw;
	
file_put_contents($confAvailableDir . $service, $conf);

$services[] = $service;

foreach ($services as $confFile) {
	shell_exec('ln -s ' . $confAvailableDir . $confFile . ' ' . $confEnabledDir);
}

// Restart Monit to apply new configuration
shell_exec('systemctl restart monit');

print('[INFO] Your Monit admin password is ' . $monitpw  . "\n");
?>